import EmberRouter from "@ember/routing/router";
import config from "./config/environment";

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  //this.authenticatedRoute('index');
  this.authenticatedRoute("categories");
  this.authenticatedRoute("prog");
  this.authenticatedRoute("prog");
  this.authenticatedRoute("queue");
  this.authenticatedRoute("complete");
  this.authenticatedRoute("login");
  this.authenticatedRoute("test");
  this.authenticatedRoute("clipstream");
  this.authenticatedRoute("logs");

  this.route("howl");
  this.route("flow");
  this.route("audio", {
    path: "audio/:id"
  });
  this.route("audioclip", {
    path: "/:id"
  });
  this.route("list");

  this.route("wave", {
    path: "wave/:id"
  });
  this.route("map");
  this.route(
    "meta",
    {
      path: "meta/:id"
    },
    function() {
      this.route("categories");
    }
  );
  this.route("categories");
  this.route("categorise", function() {});
  this.route("users");
  this.route("login");
  this.route("test");
  this.route("clipstream");

  this.route("prog", {}, function() {
    this.route("clip", {
      path: "/:id"
    });
  });
  this.route("queue");
  this.route("complete");

  this.route("flashbriefing", function() {
    this.route("video");
    this.route("add");
    this.route("settings", {
      path: "/:category"
    });
  });
  this.route("logs");
  this.route("iframe", function() {
    this.route("manage");
  });
  this.route("post");
  this.route("manage");
  this.route("manage-iframe", function() {});
});

export default Router;
