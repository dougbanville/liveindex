import Controller from '@ember/controller';
import { computed } from '@ember/object';


export default Controller.extend({

    init(){
        this._super(...arguments);
        this.sorting = ['starttime:desc'];
    },

    sorttedModel: computed.sort("model","sorting"),

    editing: computed.filter("sorttedModel.@each.publishStatus",function(r,index, array){

        return r.publishStatus === "editing";

    }),

    processing: computed.filter("sorttedModel.@each.publishStatus",function(r,index, array){

        return r.publishStatus === "processing";

    }),

    complete: computed.filter("sorttedModel.@each.publishStatus",function(r,index, array){

        return r.publishStatus === "complete";

    }),

    actions:{
        deleteClip(model){
            model.destroyRecord().then(_=>{
            })
        }
    }
});
