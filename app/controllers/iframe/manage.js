import Controller from '@ember/controller';

export default Controller.extend({

    actions:{
        add(){
            let newReport = this.store.createRecord("report",{
                title: this.get("title"),
                url: this.get("url")
            })

            newReport.save().then(()=>{
                alert("ok")
            })
        },
        update(model){
            model.set("title",model.title);
            model.set("url",model.url);
            model.save();
        }
    }
});
