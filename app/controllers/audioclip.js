import Controller from '@ember/controller';

export default Controller.extend({

    showWave: true,
    
    actions:{
        toggleShowWave(){
			this.toggleProperty("showWave");
		}
    }
});
