import Controller from '@ember/controller';

export default Controller.extend({


    actions:{
        redirect(){
            let id = this.get("model").get("id");
            this.transitionToRoute("meta",id);
        }
    }
});
