import Controller from '@ember/controller';

export default Controller.extend({

    actions:{
        deleteRecord(model){

            let category = model.get("category");
            console.log(category)
            
            model.destroyRecord().then(()=>{
                category.save()
            });
            
        }
    }
});
