import Controller from '@ember/controller';
import { inject as service } from '@ember/service';

export default Controller.extend({

    session: service(),

    firebaseApp: service(),

    actions: {
        login() {
            this.get('session').open('firebase', {
                provider: 'password',
                email: this.userEmail,
                password: this.userPassword
            }).then(r=>{
                console.log(r)
            }).catch(e=>{
                this.set("loginError",e.message)
            })
        },
        logout(){
            this.get('session').close();
        },
        resetPassword(){
            let auth = this.get("firebaseApp").auth();
            auth.sendPasswordResetEmail(this.userEmail).then(r=>{
                this.set("loginError","Check your email")  
            }).catch(e=>{
                this.set("loginError",e.message)
            })
        }
    }
});
