import Controller from '@ember/controller';
import { match, not, gte, and } from '@ember/object/computed';
import { inject as service } from '@ember/service'

export default Controller.extend({

    firebaseApp: service(),

    userEmail: "",

    userPassword: "",

    isValid: match('userEmail', /^.+@.+\..+$/),

    isDisabled: not('isValid'),

    isStrongPassword: gte("userPassword.length",5),

    isBothTrue: and('isValid', 'isStrongPassword'),

    userError: null,


    actions:{
        createFirebaseUser() {
            const auth = this.get("firebaseApp").auth();
            return auth.createUserWithEmailAndPassword(this.userEmail, this.userPassword).catch(e=>{
                this.set("userError", e.message);
            })
          }
    }


});
