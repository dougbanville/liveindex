import Controller from '@ember/controller';
import moment from 'moment';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';
import ENV from '../config/environment';


export default Controller.extend({
    ajax: service(),
    stations: service(),

    queryParams: ['category','isFlashBriefing','service'],

    options: {
        timeFormat: 'HH:mm:ss'
    },

    center: new Date(),

    selected: moment(),

    duration: 0,

    minDate: new Date("2018-11-01"),
    maxDate: new Date(),

    showCalendar: false,

    format: "mp3",
    fileName: "radio1",
    minutes: 5,
    buttonState: "",
    isUploading: false,
    time: null,
    previewUrl: null,
    showPreview: false,
    categoryChosen: null,

    playAudio: (start, end, service) => {

        let ap = document.getElementById("audioPlayer");
        //ap.play(); start >= end || 
        if (start >= moment()) {
            alert("Check the length of clip it's not long enough or the date is in the future")
        } else {
            let afUrl = `http://audiofile.rte.ie/webservice/v3/download.php?start=${moment.utc(start).format("YYYY-MM-DD-HH-mm-ss-ss")}&end=${moment.utc(end).format("YYYY-MM-DD-HH-mm-ss-ss")}&service=${service}`; //2018-08-20-07-59-00-00
            ap.src = afUrl;
            ap.play();
        }

    },

    playEnd: function (end, service) {
        this.playAudio(moment.utc(end).subtract(3, "seconds"), moment.utc(end), service);
    },

    actions: {
        chooseCategory(category) {
            this.set("category", category)
        },
        onChange(selectedTime) {
            let time = moment(selectedTime).format("HH:mm:ss");
            this.set("time", time)
            let day = moment(this.get("selected"));
            let newDate = day.format("YYYY-MM-DD") + " " + time;
            newDate = moment(newDate);
            this.set("start", newDate);
            let end = moment(newDate).add(this.get("minutes"), "minutes");
            this.set("end", moment(end));
            let afUrl = `http://audiofile.rte.ie/webservice/v3/download.php?start=${moment.utc(this.get("start")).format("YYYY-MM-DD-HH-mm-ss-ss")}&end=${moment.utc(this.get("end")).format("YYYY-MM-DD-HH-mm-ss-ss")}&service=${this.get("stations").get("selectedStation").station_name}`; //2018-08-20-07-59-00-00
            this.set("previewUrl", afUrl);
            this.set("showPreview", true)
        },
        addDuration() {
            this.set("showPreview", true)
            let end = moment(this.get("start")).add(this.get("minutes"), "minutes");
            this.set("end", end);
            let afUrl = `http://audiofile.rte.ie/webservice/v3/download.php?start=${moment.utc(this.get("start")).format("YYYY-MM-DD-HH-mm-ss-ss")}&end=${moment.utc(this.get("end")).format("YYYY-MM-DD-HH-mm-ss-ss")}&service=${this.get("stations").get("selectedStation").station_name}`; //2018-08-20-07-59-00-00
            this.set("previewUrl", afUrl);
            this.set("showPreview", true)

        },
        updateDate(selectedDate) {
            let date = moment(selectedDate)
            let selected = date.format("YYYY-MM-DD") + " " + this.get("time");
            this.set("selected", moment(selected));
            this.set("start", selected);
            let end = moment(selected).add(this.get("minutes"), "minutes");
            this.set("end", moment(end));
        },
        setEnd() {
            let start = this.get("start");
            let end = moment(start).add(this.get("minutes"), "minutes");
            this.set("end", moment(end));
        },
        saveAudioFile() {
            let stationId = this.get("stations").get("selectedStation").id;
            
            let start = moment(this.get("start"));
            let end = moment(start).add(this.get("minutes"), "minutes");
            this.set("end", moment(end));
            let timeRequested = moment(start);
            //get schedule info
            let url = `https://feeds.rasset.ie/rtelistings/cal/${stationId}/${timeRequested.format("YYYY/MMM/DD").toLowerCase()}/?callback=html5radioplayer`;
            this.get("ajax").request(url, {
                dataType: "jsonp"
            }).then(r => {
                r.filter(s=>{
                    let start = moment(s.fields.progdate);
                    let end = moment(s.fields.progenddate);
                    if(timeRequested >= start && timeRequested < end){
                        let imageRef = s.fields.imageref.split('/').pop();
                        imageRef = imageRef.replace(".jpg","");
                        var newAudioClip = this.store.createRecord('audioclip', {
                            starttime: new Date(this.get("start")),
                            endtime: new Date(this.get("end")),
                            title: s.fields.progtitle,
                            picture: `https://img.rasset.ie/${imageRef}-1200.jpg`,
                            imageRef: imageRef,
                            flashBriefing: this.get("isFlashBriefing"),
                            category: this.get("category"),
                            service: this.get("stations").get("selectedStation").station_name
                        });
                        newAudioClip.save().then((r) => {
                            this.transitionToRoute('audio', r.id)
                        })
                    }
                })
            }).catch(e => {
                console.log(e)
            })
        },
        twistedAudioFile() {
            this.set("buttonState", "disabled");
            this.set("isUploading", true);
            let start = moment(this.get("start"));
            let end = moment(start).add(this.get("minutes"), "minutes");
            this.set("end", moment(end));
            let service = this.get("stations").get("selectedStation").station_name;
            let format = this.get("format");
            let fileName = `${moment().format("YYYYMMDDHHmmssSS")}${this.get("stations").get("selectedStation").station_name}.${format}`;
            let s3UploadUrl = `${ENV.transcoderEndpoint}/twisted.php?start=${moment.utc(this.get("start")).format("YYYY-MM-DD-HH-mm-ss-ss")}&end=${moment.utc(this.get("end")).format("YYYY-MM-DD-HH-mm-ss-ss")}&service=${this.get("stations").get("selectedStation").station_name}&format=${format}&fileName=${fileName}`;
            let twisted = this.get("ajax").request(s3UploadUrl, {
                dataType: "jsonp",
            }).then(r => {
                this.set("tw_token", r.edit.token);

                var newAudioClip = this.store.createRecord('audioclip', {
                    starttime: new Date(this.get("start")),
                    endtime: new Date(this.get("end")),
                    tw_token: r.edit.token,
                    tw_jobId: r.id,
                    category: this.get("category"),
                    title: this.get("categoryChosen").name,
                    outputFileName: `https://s3-eu-west-1.amazonaws.com/tw-audio-out/${fileName}`,
                    awsaudio: `https://s3-eu-west-1.amazonaws.com/tw-audio-out/${fileName}`,
                    awsKey: fileName
                });
                newAudioClip.save().then((r) => {
                    this.set("isUploading", false);
                    //this.set("categoryChosen", null);
                    this.transitionToRoute('wave', r.id)
                })

            })
        },
        toggleCal() {
            this.toggleProperty("showCalendar")
        },
        play() {
            this.playAudio(this.get("start"), this.get("end"), this.get('service'));
            this.set("isPlaying", true)
        },
        playEnd() {
            let endPoint = this.get("end");
            //console.log(endPoint.subtract(10,"seconds").format("DD MMM HH:mm:ss"), endPoint.add(10,"seconds").format("DD MMM HH:mm:ss"));
            //this.playAudio(endPoint.add(20,"seconds"),endPoint.subtract(10,"seconds"),this.get("stations").get("selectedStation").station_name);
            this.playEnd(this.get("end"), this.get("stations").get("selectedStation").station_name)
            this.set("isPlaying", true)
        },
        pause() {
            let ap = document.getElementById("audioPlayer");
            ap.pause();
            this.set("isPlaying", false)
        },
        seek(seconds, direction, point) {
            let newStart;
            if (direction === "forward") {
                newStart = moment(this.get(point)).add(seconds, "seconds");
            } else {
                newStart = moment(this.get(point)).subtract(seconds, "seconds");
            }
            this.set(point, moment(newStart));
            let time = moment(newStart).format("HH:mm:ss");
            this.set("time", time);
            this.playAudio(this.get("start"), this.get("end"), this.get('service'));

        },
        updateStartTime(seconds, direction, point) {
            let newStart;
            if (direction === "forward") {
                newStart = moment(this.get(point)).add(seconds, "seconds");
            } else {
                newStart = moment(this.get(point)).subtract(seconds, "seconds");
            }
            this.set(point, moment(newStart));
            let time = moment(newStart).format("HH:mm:ss");
            this.set("time", time);
        }
    }
});