import Controller from '@ember/controller';

export default Controller.extend({

    actions:{
        saveModel(){
            const model = this.get("model");

            model.save();
        }
    }
});
