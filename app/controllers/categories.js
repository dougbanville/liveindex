import Controller from '@ember/controller';
import slugify from 'slugify'

export default Controller.extend({

    actions:{
        addCategory(){
            let categorySlug = slugify(this.get("categoryName"),{
                lower: true
            });
            let newCategory = this.store.createRecord('category',{
                id: categorySlug,
                slug: categorySlug,
                name: this.get("categoryName")
            })
            
            newCategory.save().then(()=>{
                //alert("saved")
            })
        },
        delete(category){
            category.destroyRecord();
        },
        updateCategory(category){
            category.save();
        },
    }
});
