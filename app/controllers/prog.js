import Controller from '@ember/controller';
import moment from 'moment';
import ENV from '../config/environment';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';
import slugify from 'slugify';
import { run, later } from '@ember/runloop';

export default Controller.extend({



    ajax: service(),
    stations: service(),
    flashMessages: service(),

    start:null,
    end:null,
    title: null,
    showWave: true,
    queryParams: ['date'],
    date: moment().format("YYYY-MM-DD"),
    sorttedModel: computed.sort("model","sorting"),

    init(){
        this._super(...arguments);
        this.sorting = ['starttime:desc'];
    },

    audio: computed.filter("sorttedModel", function(r,index, array){
        return moment(r.starttime) >= moment(this.get("date")).startOf("Day");
    }),

    actions:{
        markStart(){
            this.set("recording",true)
            //document.getElementById("start").disabled = true;

            let stationId = this.get("stations").get("selectedStation").id;

            //get schedule info
            this.get("ajax").request(`https://feeds.rasset.ie/rtelistings/cal/${stationId}/delta/1543831123/?&callback=html5radioplayer`, {
                dataType: "jsonp"
            }).then(r=>{
                this.set("currentProg",r[0].fields)
            }).catch(e=>{
                this.set("error",e)
            })
            this.set("start",moment());
        },
        makeClip(){
            document.getElementById("end").disabled = true;
            this.set("end",moment())
            let audioFileUrl = `${ENV.audioFileEnpoint}/webservice/v3/download.php?start=${moment.utc(this.get("start")).format("YYYY-MM-DD-HH-mm-ss-ss")}&end=${moment.utc(this.get("end")).format("YYYY-MM-DD-HH-mm-ss-ss")}&service=${this.get("stations").get("selectedStation").station_name}`;

            this.set("audioFileUrl",audioFileUrl);
            let slug = slugify(this.get("currentProg").progname,{
                lower: true,
                remove: /[*+~.()'"!:@]/g
            });
           
            let imageRef = this.get("currentProg").imageref.split('/').pop();
            imageRef = imageRef.replace(".jpg","");


            var newAudioClip = this.store.createRecord('audioclip', {
				starttime:  new Date(this.get("start")),
                endtime: new Date(this.get("end")),
                title: this.get("currentProg").progname,
                category: slug,
                service: this.get("stations").get("selectedStation").station_name,
                picture: `https://img.rasset.ie/${imageRef}-1200.jpg`,
                imageRef: imageRef
            });

            newAudioClip.save().then(r=>{
                this.set("recording",false);
                this.set("end",null);
                if(this.get("redirect")){
                    this.transitionToRoute('audio', r.id);
                }
            })
        },
        deleteClip(model){
            model.destroyRecord().then(_=>{
                this.get("flashMessages").success("Deleted");
            })
        }
    }

});
