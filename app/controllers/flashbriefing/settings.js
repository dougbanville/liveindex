import Controller from '@ember/controller';
import { inject as service } from '@ember/service';

export default Controller.extend({

    flashMessages: service(),

   actions:{
    save(){
        let model = this.get("model");
        model.save().then(r=>{
            this.get("flashMessages").success("Saved");
        })
    }
   }
});
