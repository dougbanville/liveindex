import Controller from '@ember/controller';
import moment  from 'moment';
import slugify from 'slugify'


export default Controller.extend({

    actions:{
        add(){
            let newBriefing = this.store.createRecord('flashBriefing',{
                id: slugify(this.get("titleText"),{
                    lower:true
                }),
                titleText: this.get("titleText"),
                mainText: this.get("mainText"),
                hasVideo: this.get("hasVideo"),
                lastUpdated: new Date(moment().subtract(1,"days").format())
            })

            newBriefing.save().then(_=>{
                this.transitionToRoute('flashbriefing.index');
            })
        }
    }
});
