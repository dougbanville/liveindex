import Controller from '@ember/controller';
import { computed } from '@ember/object';
import moment from 'moment';
import { inject as service } from '@ember/service';
import ENV from '../../config/environment';

export default Controller.extend({

    ajax: service(),

    sortedVideos: computed.sort("video","sortOrder"),

    sortedBriefings: computed.sort("flashBriefings","fbSort"),

    videoNeedsUpdate: computed("videos",function(){
        return this.get("videos").firstObject.created;
    }),

    init(){
        this._super(...arguments);
        this.set("sortOrder",['created:desc']);
        this.set("fbSort",['sortOrder:desc']);
    },

    latestStart: computed(function(){
        return moment().startOf("hour");
    }),

    latestEnd: computed("latestStart",function(){
        return moment().startOf("hour").add(5,"minutes");
    }),
    //TODO Make a timer to check this
    needsUpdate: computed("model",function(){
        let latest = moment(this.get("model").firstObject.starttime);
        let gap = moment().diff(latest,"minutes");
        if(gap > 65){
            return true
        }
    }),

    actions:{
        redirect(id){
            this.transitionToRoute('audio', id)
        },
        makeFlashAudio(){
            var newAudioClip = this.store.createRecord('audioclip', {
				starttime:  new Date(this.get("latestStart")),
                endtime: new Date(this.get("latestEnd")),
                title: "news",
                category: "flash-briefing-news",
                service: "2fm",
                picture: "https://img.rasset.ie/000a3163-1200.jpg",
                flashBriefing: true
            });

            newAudioClip.save().then(r=>{
                this.transitionToRoute('audio', r.id)
            })
        },
        publishVideo(videoUrl){
            this.set("savingVideo",true);
            let model = this.get("model").firstObject;
            let audioId = this.get("model").firstObject.id;
            
            model.set("videoUrl",videoUrl);
            let jsonpCallback ="x";
            let s3UploadUrl = `${ENV.transcoderEndpoint}transcoder/saveVideo.php?audioId=${audioId}&fileName=${videoUrl}&callback=${jsonpCallback}`;

            model.save().then(_=>{
               
                this.get("ajax").request(s3UploadUrl, {
                    dataType: "jsonp",
                    jsonpCallback: jsonpCallback
                }).then(r=>{
                    console.log(r);
                    this.set("savingVideo",false);
                }).catch(e=>{
                    console.log("error");
                })
           })
           
        },
        refreshRoute(){
            this.transitionToRoute("flashbriefing.index");
        }
    }
});
