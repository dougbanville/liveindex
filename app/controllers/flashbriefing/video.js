import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';


export default Controller.extend({

    ajax: service(),

    sortOrder: null,

    videos: computed.sort("model","sortOrder"),

    init(){
        this._super(...arguments);
        this.set("sortOrder",['created:desc']);
    }


    
});
