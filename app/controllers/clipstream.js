import Controller from '@ember/controller';
import moment from 'moment';

export default Controller.extend({

    duration: 5,

    didInsertElement(){

        let audio = document.getElementById("audio");
        this.set("audioPlayer",audio);

    },

    actions:{
        onChange(selectedTime) {
            let time = moment(selectedTime).format("HH:mm:ss");
            let day = moment();
            let newDate = day.format("YYYY-MM-DD") + " " + time;
            console.log(moment(newDate).format("X"));
            console.log(moment.duration(5,"minutes").asSeconds());
            let streamUrl = `https://sgrewind.streamguys1.com/rte/radio1/playlist_dvr_range-${ moment(newDate).format("X")}-${moment.duration(5,"minutes").asSeconds()}.m3u8`;
            console.log(streamUrl);
            this.get("audioPlayer").src = streamUrl;
        },
    }
});
