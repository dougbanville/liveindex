import Controller from '@ember/controller';
import moment from 'moment';
import { inject as service } from '@ember/service';
import ENV from '../config/environment';


export default Controller.extend({
    ajax: service(),

    options: {
        dropdown: true,
        minTime: new Date(0, 0, 0, 7, 0, 0),
        maxTime: new Date(0, 0, 0, 15, 0, 0),
        
    },

    init(){
        this._super(...arguments);
    },

    service: "radio1",
    format: "mp3",
    fileName: "testing",
    minutes: 0,


    actions: {
        onChange(selectedTime) {
            let time = moment(selectedTime).format("h:m");
            let newDate = moment().format("YYYY-MM-DD") + " " + time;
            newDate = moment(newDate);
            console.log(newDate.format("YYYY-MM-DD-hh-mm-ss-SS"));
            this.set("start",newDate);
            
        },
        setEnd(){
            let start = this.get("start");
            console.log(`${this.get("minutes")}`);
            let end = moment(start).add(this.get("minutes"),"minutes");
            this.set("end",moment(end));
            console.log(moment(end).format("YYYY-MM-DD-hh-mm-ss-SS"));
        },
        saveAudioFile(){
            let start = moment(this.get("start")).format("YYYY-MM-DD-hh-mm-ss-SS");
            let end = moment(this.get("end")).format("YYYY-MM-DD-hh-mm-ss-SS");
            let service = this.get("service");
            let format = this.get("format");
            let fileName = this.get("fileName");
            let s3UploadUrl = `http://localhost/transcoder?start=${start}&end=${end}&service=${service}&format=${format}&fileName=${fileName}`;
            
            console.log(s3UploadUrl);
            let afUrl =  `http://audiofile.rte.ie/webservice/v3/download.php?start=${moment.utc(this.get("start")).format("YYYY-MM-DD-HH-mm-ss-ss")}&end=${moment.utc(this.get("end")).format("YYYY-MM-DD-HH-mm-ss-ss")}&service=${this.get("service")}`; //2018-08-20-07-59-00-00
            console.log(afUrl);

            var newAudioClip = this.store.createRecord('audioclip', {
				starttime:  new Date(this.get("start")),
				endtime: new Date(this.get("end")),
            });
            newAudioClip.save().then((r)=>{
                //alert(":saved" + r.id);
                this.transitionToRoute('audioclip',r.id)
            })
        }
    }
});
