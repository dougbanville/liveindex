import Controller from '@ember/controller';
import moment from 'moment';
import momentDurationFormatSetup from 'moment-duration-format';
import ENV from '../config/environment';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
/*
(Optional) Clip Duration
The duration of your excerpt clip. The format can be either HH:mm:ss.SSS (maximum value: 23:59:59.999; SSS is thousandths of a second) or sssss.SSS (maximum value: 86399.999). If you don't specify a value, Elastic Transcoder clips from Clip Start Time to the end of the file.

If you specify a value longer than the duration of the input file, Elastic Transcoder transcodes from Clip Start Time to the end of the file and returns a warning message.
*/


export default Controller.extend({

    init(){
        this._super(...arguments);
        momentDurationFormatSetup(moment);
    },

    showWave: true,

    ajax: service(),

    working: false,

    cloudFunctionUrl: ENV.firebase.projectId,

    shareUrl: computed("model", "cloudFunctionUrl",function(){
        return `https://us-central1-${this.get("cloudFunctionUrl")}.cloudfunctions.net/socialTags/${this.get("model").id}`;
    }),


    actions:{
        toggleShowWave(){
			this.toggleProperty("showWave");
        },
        saveWave(){
            let model = this.get("model");
            model.set("publishStatus","clip");
            model.save().then(_=>{
                alert("done")
            })
        },
        apiRequest(model){
            this.set("working",true);
            let awsUrl = `https://audiofile-trimmed.s3.amazonaws.com/`;
            let start = moment.utc(model.get("starttime")).format("YYYY-MM-DD-HH-mm-ss-SS");
            let end = moment.utc(model.get("endtime")).format("YYYY-MM-DD-HH-mm-ss-SS");
            let service = model.get("service");
            let fileFormat = "mp3";
            let fileName = moment().format("X");
            let audioInTime = model.get("audioInTime"); //sssss.SSS 
            let audioOutTime = model.get("audioOutTime");
            audioOutTime = audioOutTime.toFixed(3);
            audioOutTime = parseFloat(audioOutTime);
            let duration = audioOutTime - audioInTime;
            duration = moment.duration(duration, "seconds").format("HH:mm:ss.SSS");
            //duration = "00:00:03.446" //00:00:03.446
            let jsonpCallback = "test";
            let s3UploadUrl = `${ENV.transcoderEndpoint}transcoder/?start=${start}&end=${end}&service=${service}&format=${fileFormat}&fileName=${fileName}&audioIn=${audioInTime}&audioOut=${audioOutTime}&duration=${duration}&callback=${jsonpCallback}`;
                    
            this.get("ajax").request(s3UploadUrl, {
                dataType: "jsonp",
                //jsonpCallback: jsonpCallback
            }).then(r=>{
                this.set("working",false);
                let response = r
                let trimmed = `${awsUrl}${response.body.Job.Output.Key}`;
                this.set("trimmedAudio",trimmed);
                model.set("awsaudio",trimmed);
                model.set("published",true);
                model.set("publishStatus","transcoded");
                model.save();
                let jobId = response.body.Job.Id;
                //this.checkJobStatus(jobId);                
            }).catch(e=>{
                console.log(e)
                alert("Error!");
            })
        },
        phpRequest(model){
            this.set("working",true);
            let start = moment.utc(model.get("starttime")).format("YYYY-MM-DD-HH-mm-ss-SS");
            let end = moment.utc(model.get("endtime")).format("YYYY-MM-DD-HH-mm-ss-SS");
            let service = model.get("service");
            let fileFormat = "mp3";
            let fileName = moment().format("X");
            let audioInTime = model.get("audioInTime"); //sssss.SSS 
            let audioOutTime = model.get("audioOutTime");
            let duration = audioOutTime - audioInTime;
            duration = moment.duration(duration, "seconds").format("HH:mm:ss.SSS");
            let jsonpCallback = "test";
            let s3UploadUrl = `${ENV.transcoderEndpoint}transcoder/?audioId=${model.get('id')}&start=${start}&end=${end}&service=${service}&format=${fileFormat}&fileName=${fileName}&audioIn=${audioInTime}&audioOut=${audioOutTime}&duration=${duration}&callback=${jsonpCallback}`;
            model.set("publishStatus","processing");
            model.save().then(_=>{
                this.get("ajax").request(s3UploadUrl, {
                    dataType: "jsonp",
                    jsonpCallback: jsonpCallback
                }).then(r=>{
                    this.set("working",false);
                }).catch(e=>{
                    console.log("error");
                })
            })
        },
        crop(model){ //TODO this does not work
            this.set("showWave",false);

            let start = moment.utc(model.get("starttime")).format("YYYY-MM-DD-HH-mm-ss-SS");
            let audioInTime = model.get("audioInTime");
            let audioOutTime = model.get("audioOutTime");
            let cropStart = moment.utc(model.get("starttime")).add(parseInt(audioInTime).toFixed(),"seconds");
            let cropEnd = moment.utc(model.get("endtime")).subtract(parseInt(audioOutTime).toFixed(),"seconds");

           
            model.set("starttime",new Date(cropStart));
            model.set("endtime",new Date(cropEnd));

            console.log(moment(cropStart).format("YYYY-MM-DD-HH-mm-ss-SS"));
            
            model.save().then(_=>{
                this.set("showWave",true);
            })
            
           
        },
        autoSave(){
            this.get("model").save();
        }
    }
});
