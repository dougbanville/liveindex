import Controller from "@ember/controller";
import { inject as service } from "@ember/service";

export default Controller.extend({
  display: service(),
  actions: {
    add() {
      //console.log(this.display.toggleShow);
      this.display.toggleShow(true);
      this.send("toggleDisplay", false);
      let newReport = this.store.createRecord("report", {
        title: this.get("title"),
        url: this.get("url")
      });

      newReport.save().then(() => {
        this.send("toggleDisplay", false);
      });
    },
    delete(model) {
      this.send("toggleDisplay", true);
      model.destroyRecord().then(() => {
        this.send("toggleDisplay", false);
        //alert("ok");
      });
    },
    toggleDisplay(display) {
      let settings = this.get("settings");
      settings.set("hideSliders", display);
      settings.save();
    }
  }
});
