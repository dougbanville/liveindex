import Controller from '@ember/controller';
import { computed } from '@ember/object';

export default Controller.extend({

    sortedLogs: computed.sort("model","sortOrder"),

    init(){
        this._super(...arguments);
        this.set("sortOrder",['time:desc']);
    },
});
