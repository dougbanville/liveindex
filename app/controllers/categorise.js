import Controller from '@ember/controller';
import { inject as service } from '@ember/service';

export default Controller.extend({

    firebaseApp: service(),

    actions:{
        addToCategory(category,audio){
            //alert(category);
            let db = this.get('firebaseApp').database();

            let audioId = audio.get("id");

            db.ref(`audioInCategories/${category}`).child(audioId).set({
                audioclip: {
                    [audioId] : true
                }
            })

            
        }
    }
});


