import Controller from '@ember/controller';
import moment from 'moment';
import { inject as service } from '@ember/service';
import ENV from '../config/environment';


export default Controller.extend({
    ajax: service(),
    stations: service(),
    flashMessages: service(),
    audio: service(),

    options: {
        dropdown: true,
        minTime: new Date(0, 0, 0, 7, 0, 0),
        maxTime: new Date(0, 0, 0, 15, 0, 0),
        
    },

    init(){
        this._super(...arguments);
    },

    service: "radio1",
    format: "mp3",
    fileName: "testing",
    minutes: 0,


    actions: {
        onChange(selectedTime) {
            let time = moment(selectedTime).format("h:m");
            let newDate = moment().format("YYYY-MM-DD") + " " + time;
            newDate = moment(newDate);
            this.set("start",newDate);
            
        },
        setEnd(){
            let start = this.get("start");
            let end = moment(start).add(this.get("minutes"),"minutes");
            this.set("end",moment(end));
        },
        saveAudioFile(){
            let start = moment(this.get("start")).format("YYYY-MM-DD-hh-mm-ss-SS");
            let end = moment(this.get("end")).format("YYYY-MM-DD-hh-mm-ss-SS");
            let service = this.get("service");
            let format = this.get("format");
            let fileName = this.get("fileName");
            let s3UploadUrl = `http://localhost/transcoder?start=${start}&end=${end}&service=${service}&format=${format}&fileName=${fileName}`;
            
            let afUrl =  `http://audiofile.rte.ie/webservice/v3/download.php?start=${moment.utc(this.get("start")).format("YYYY-MM-DD-HH-mm-ss-ss")}&end=${moment.utc(this.get("end")).format("YYYY-MM-DD-HH-mm-ss-ss")}&service=${this.get("service")}`; //2018-08-20-07-59-00-00

            var newAudioClip = this.store.createRecord('audioclip', {
				starttime:  new Date(this.get("start")),
				endtime: new Date(this.get("end")),
            });
            newAudioClip.save().then((r)=>{
                //alert(":saved" + r.id);
                this.transitionToRoute('audioclip',r.id)
            })
        },
        setService(station){
        },
        dismissError(){

            this.get("audio").setError(false);

        }
    }
});
