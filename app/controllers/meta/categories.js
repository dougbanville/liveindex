import Controller from '@ember/controller';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';


export default Controller.extend({

    firebaseApp: service(),

    getAudioInCategory: function(category,callback){
        let db = this.get("firebaseApp").database();
        let categoryRef = db.ref(`categories/${category}/audioclip`);
        let audioRef = db.ref("audioclip");

        db.ref(`audioInCategories/${category}`).on("child_added",snap=>{
            let audio = db.ref(`audioclips`).child(snap.key);
            audio.once('value',callback)
        })
    },

    audioInCategory: computed("getAudioInCategory",function(){
        return this.getAudioInCategory("weather", snap=>{
            console.log(snap.val());
            //return snap.val();
            this.set("categorised", snap.val())
        });
    }),

    actions:{
        addClipToCategory(category){
            //alert("ok");
            let audioClip = this.get("audioClip");
           
            console.log(audioClip.get("title"))
            category.get("audioclip").addObject(audioClip);
            category.save().then(function(){
                audioClip.set("category",category)
                return audioClip.save()
            })
            //console.log(category.id + audioClip.title)
        },
        removeClipFromCategory(category,audioclip){
            //category.get("audioclip").removeObject(audioclip);
            //category.get("audioclip").addObject(audioClip);
            
        }
    }
});
