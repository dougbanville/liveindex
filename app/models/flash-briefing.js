import DS from 'ember-data';

export default DS.Model.extend({

    mainText: DS.attr("string"),
    redirectionUrl: DS.attr("string"),
    streamUrl: DS.attr("string"),
    titleText: DS.attr("string"),
    uid: DS.attr("string"),
    lastUpdated: DS.attr("date"),
    hasVideo: DS.attr("boolean"),
    videoUrl: DS.attr("string"),
    videoSourceFile: DS.attr("string"),
    gain: DS.attr("number"),
    service: DS.attr("string"),
    videoPublishDate: DS.attr("date"),
    clipHourly: DS.attr("boolean"),
    sortOrder: DS.attr("number", { defaultValue(){ return 0 } }),
    duration: DS.attr("string"),
    image: DS.attr("string"),
    category: DS.attr("string"),
    fileSize: DS.attr("string")
});
