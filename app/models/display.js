import DS from "ember-data";

export default DS.Model.extend({
  displayName: DS.attr("string"),
  showAlert: DS.attr("boolean"),
  alertTitle: DS.attr("string"),
  alertText: DS.attr("string"),
  hideSliders: DS.attr("boolean"),
  showTakeOver: DS.attr("boolean"),
  takeOverHtml: DS.attr("string"),
  interval: DS.attr("number")
});
