import DS from 'ember-data';
import moment from 'moment';
import { computed } from '@ember/object';
import ENV from '../config/environment';

export default DS.Model.extend({

	title : DS.attr("string"),
	starttime : DS.attr("date"),
	endtime  : DS.attr("date"),
	picture: DS.attr("string"),
	published : DS.attr("boolean"),
	headline : DS.attr('string'),
	caption : DS.attr("string"),
	awsaudio: DS.attr("string"),
	audioInTime: DS.attr("number"),
	audioOutTime: DS.attr("number"),
	service: DS.attr("string",{ defaultValue: "radio1_p" }),
	tw_token: DS.attr("string"),
	tw_jobId: DS.attr("string"),
	tw_jobComplete: DS.attr("boolean"),
	awsKey: DS.attr("string"),
	outputFileName: DS.attr("string"),
	awsUploadComplete: DS.attr('boolean', { defaultValue: false }),
	publishStatus: DS.attr("string",{ defaultValue: "editing" }),
	publishDate: DS.attr("date"),
	//category: DS.belongsTo('category', { async: false, inverse: null }),
	clipperFilename: DS.attr("string"),
	category: DS.attr("string"),
	videoUrl: DS.attr("string"),
	awsVideo: DS.attr("string"),
	imageRef: DS.attr("string"),
	generatedVideo: DS.attr("string"),
	audioFileUrl: computed("starttime","endtime","service",function(){
		let service = this.get("service");
		if(service === "radio1"){
			service = "radio1_p";
		}
		return `${ENV.audioFileEnpoint}/player/?start=${moment.utc(this.get("starttime")).format("YYYY-MM-DD-HH-mm-ss-SS")}&end=${moment.utc(this.get("endtime")).format("YYYY-MM-DD-HH-mm-ss-SS")}&service=${service}`; //2018-08-20-07-59-00-00
	}),
	getAudioUrl: computed("starttime","endtime","service",function(){
		let service = this.get("service");
		if(service === "radio1"){
			service = "radio1_p";
		}
		return `${ENV.audioFileEnpoint}/webservice/v3/download.php?service=${service}&start=${moment.utc(this.get("starttime")).format("YYYY-MM-DD-HH-mm-ss-SS")}&end=${moment.utc(this.get("endtime")).format("YYYY-MM-DD-HH-mm-ss-SS")}&file_title=doug.mp3&format=mp3`
	}),
	flashBriefing: DS.attr('boolean')

});
