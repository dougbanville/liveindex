import DS from 'ember-data';

export default DS.Model.extend({
    message: DS.attr('string'),
    title: DS.attr("string"),
    type: DS.attr("string")
});
