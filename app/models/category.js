import DS from 'ember-data';

export default DS.Model.extend({

    slug: DS.attr("string"),
    name: DS.attr("string"),
    link: DS.attr("string"),
    audioclip: DS.hasMany("audioclip",{ async: true, inverse: null })
});
