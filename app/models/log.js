import DS from 'ember-data';

export default DS.Model.extend({

    log: DS.attr("string"),
    time: DS.attr("date"),
    type: DS.attr("string")

});
