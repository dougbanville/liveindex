import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default Route.extend({

  stations: service(),

    model(){
      let stationId = this.get("stations").selectedStation;
      stationId = stationId.replace("_p","");
      let id = this.get("stations").getStationNumericId(stationId);
      return this.get("stations").getStation(id);
    },

    session: service(),
    beforeModel: function() {
      return this.get('session').fetch().catch(function() {});
    },

    actions: {
      accessDenied: function() {
        this.transitionTo('login');
      }
    }
  
});
