import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';


export default Route.extend({

    audio: service(),

    model(params){
        return this.store.findRecord("audioclip",params.id).catch((e)=>{
            console.log("Error");
            //this.set("error",e);
            this.get("audio").setError(e);
        });
    },

});
