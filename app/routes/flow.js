import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';


export default Route.extend({
    flashMessages: service(),

    actions:{
        note(){
            const flashMessages = this.get('flashMessages');
            flashMessages.danger('Something went wrong!');
        }
    }
});
