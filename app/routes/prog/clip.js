import Route from '@ember/routing/route';

export default Route.extend({


    model(params){
        return this.store.findRecord("audioclip",params.id);
    },

    afterModel(controller){
        controller.set('showWave',true);
    },

    actions:{
        willTransition: function(transition) {
            this.controller.set('showWave',false);
          }
    }
});
