import Route from '@ember/routing/route';
import { hash } from 'rsvp';
import { inject as service } from '@ember/service';
import ENV from 'live-index/config/environment';


export default Route.extend({

    ajax: service(),

    model(){

        return hash({
            flashBriefings: this.store.findAll('flashBriefing'),
            video: this.get("ajax").request(`${ENV.transcoderEndpoint}transcoder/videoFeed.php?callback=2`,{
                dataType: "jsonp"
            }).then(r=>{
                //console.log(r)
                return r
            })
    
        })
    },

    setupController(controller,models){
        controller.set("video",models.video);
        controller.set("flashBriefings",models.flashBriefings);
    }
});
