import Route from '@ember/routing/route';

export default Route.extend({

    model(params){
        return this.store.query("flashBriefing",{
            orbderBy: "category",
            equalTo: params.category
        }).then(r=>{
            return r.firstObject;
        })
    }
});
