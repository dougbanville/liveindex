import Route from '@ember/routing/route';
import { hash } from 'rsvp';

export default Route.extend({

    model(){
        return hash({
            model: this.store.findAll("category"),
            audioClip: this.modelFor("meta")
        })
    },

    setupController(controller,models){
        controller.set("model",models.model);
        controller.set("audioClip",models.audioClip);
    }
});
