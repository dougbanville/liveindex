import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default Route.extend({

    firebaseApp: service(),

    model(){
        //return this.store.findAll("audioInCategories")
        let category = "weather";
        let db = this.get("firebaseApp").database();
        let categoryRef = db.ref(`categories/${category}/audioclip`);
        let audioRef = db.ref("audioclip");

        db.ref(`audioInCategories/${category}`).on("child_added",snap=>{
            return db.ref(`audioclips/${snap.key}`).once("value").then(r=>{
                console.log(r.val())
                return r.val()
            })
        })
    }   
});
