import Route from "@ember/routing/route";
import { hash } from "rsvp";

export default Route.extend({
  model() {
    return hash({
      model: this.store.findAll("report"),
      settings: this.store.findRecord("display", "main")
    });
  },

  setupController(controller, models) {
    controller.set("model", models.model);
    controller.set("settings", models.settings);
  }
});
