import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
import moment from 'moment';
import ENV from '../config/environment';

export default Component.extend({

    audio: service(),

    start: moment().format("YYYY-MM-DD-HH-mm-ss-SS"),

    end: moment().add(5,"minutes").format("YYYY-MM-DD-HH-mm-ss-SS"),

    didReceiveAttrs(){
        if(this.get("start")){
            let afUrl =  `${ENV.audioFileEnpoint}/webservice/v3/download.php?start=${moment.utc(this.get("start")).format("YYYY-MM-DD-HH-mm-ss-SS")}&end=${moment.utc(this.get("end")).format("YYYY-MM-DD-HH-mm-ss-SS")}&service=${this.get("service")}`; //2018-08-20-07-59-00-00
            this.set("afUrl",afUrl);
            //this.get("audioPlayer").src = this.get("arUrl");
            //this.get("audioPlayer").load();
            let duration = moment.duration(this.get("end").diff(this.get("start")));
            let audioDuration = duration.asSeconds();
            this.set("audioDuration",duration.asSeconds())
        }

    },

    didInsertElement(){
        this.set("audioPlayer", document.getElementById("audioPlayer"));
        this.set("slider",document.getElementById("slider"));
        this.get("audioPlayer").ontimeupdate = ()=>{
            this.get("audio").updateTime(this.get("audioPlayer").currentTime)
        }
        this.get("audioPlayer").onpause = ()=>{
            this.set("isPlaying",false)
        }
        this.get("audioPlayer").onplay = ()=>{
            this.set("isPlaying",true)
        }
        this.get("slider").oninput = ()=>{
            console.log(this.get("slider").value);

            this.get("audio").updateTime(this.get("audioPlayer").currentTime);
            this.get("audioPlayer").currentTime = this.get("slider").value;
            //this.get("audioPlayer").play();
        }
        this.get("audioPlayer").oncanplaythrough = ()=>{
            //alert("i am ready now")
        }
        

    },

    isPlaying: false,

    actions:{
        play(){
            this.get("audioPlayer").src = this.get("afUrl");
            this.get("audioPlayer").currentTime = this.get("audio").currentTime;//parseInt(this.get("audio").currentTime)
            console.log(this.get("audio").currentTime)
            this.get("audioPlayer").play();
            //alert(afUrl)
        },
        pause(){
            this.get("audioPlayer").pause();
            this.set("isPlaying",false)
        },
        seek(seconds, direction, point) {
            let newStart;
            if(direction === "forward"){
                newStart = moment(this.get(point)).add(seconds,"seconds");
            }else{
                newStart = moment(this.get(point)).subtract(seconds,"seconds");
            }
            this.set(point,moment(newStart));
            let time = moment(newStart).format("HH:mm:ss");
            
            this.set("time", time);

            let afUrl =  `${ENV.audioFileEnpoint}/webservice/v3/download.php?start=${moment.utc(this.get("start")).format("YYYY-MM-DD-HH-mm-ss-SS")}&end=${moment.utc(this.get("end")).format("YYYY-MM-DD-HH-mm-ss-SS")}&service=${this.get("service")}`; //2018-08-20-07-59-00-00
            console.log(afUrl)
            this.get("audioPlayer").src = afUrl;
            this.get("audioPlayer").play();
            this.get("audio").updateTime(seconds);
            //send action to controller to update form
            this.updateStartTime(seconds, direction, point);

        },
        playEnd() {
            let end = this.get("end");
            
            //!need to get the duration of clip and seek to the end
            this.get("audioPlayer").src = this.get("afUrl");

            let dur = moment.duration(moment.utc(end).subtract(3,"seconds").diff(moment.utc(this.get("start"))));
            this.get("audio").updateTime(dur.asSeconds());
            this.get("audioPlayer").currentTime = dur.asSeconds();
            this.get("audioPlayer").play();


        },
        
    }
});
