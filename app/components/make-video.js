import Component from '@ember/component';
import { inject as service } from '@ember/service';

export default Component.extend({

    ajax: service(),
    flashMessages: service(),
    audio: service(),
    show: false,

    tagName: "button",
    classNames: ["button", "success", "tiny", "float-right"],
    attributeBindings: ['disabled'],
    

    didInsertElement(){
        let model = this.get("model");

        if(model.get("generatedVideo")){
            this.set("disabled", true)
        }

    },

    click(){
        let model = this.get("model");
        let pic = `https://img.rasset.ie/${model.get("imageRef")}-700.jpg`;
        let url = `https://us-central1-${this.get("cloudFunctionUrl")}.cloudfunctions.net/makeVideo`;
        //url = `http://localhost:5000/radio-a8e0f/us-central1/makeVideo`
        this.get("audio").loadingScreenTakeOver(true);
        this.get("ajax").request(url,{
            data:{
                audio: model.get("awsaudio"),
                picture: encodeURI(pic),
                message: model.get("title")
            }
        }).then(r=>{
            this.get("audio").loadingScreenTakeOver(false);
            model.set("generatedVideo",r.videoUrl);
            model.save().then(r=>{
                this.set("show",true);
                this.get("flashMessages").success('I make a your video');
            })
        })
    },

    actions:{
        test(){
            alert("yea");
        }
    }
});
