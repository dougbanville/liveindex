import Component from '@ember/component';

export default Component.extend({

    editor: null,

    didInsertElement(){

        this.set("editor",TW.newDocumentIframe(document.getElementById("editor")));
        this.get("editor").connect(this.get("token"));

    },

    actions:{
        saveWave(){
            this.get("editor").close();
            this.redirectAction();
        }
    }
});
