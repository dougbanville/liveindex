import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';

export default Component.extend({

    stations: service(),

    showDropDown: false,

    stationList: computed("stations", function(){
        return this.get("stations").getStations()
    }),

    actions:{
        setStation(stationSelected){
           this.set("showDropDown",false);
           let station = this.get("stations").getOnAir(stationSelected);
           let header = document.getElementById("header");
           header.style.backgroundColor = station.station_color;
           this.set("selected", station);
           return station;
        }
    },

    init(){
        this._super(...arguments);
        let header = document.getElementById("header");
        let station = this.get("stations").getOnAir("radio1");
        header.style.backgroundColor = station.station_color;

        return this.get("stations").getOnAir("radio1");
    }
});
