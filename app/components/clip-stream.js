import Component from '@ember/component';
import { inject as service } from '@ember/service';
import moment from 'moment';

export default Component.extend({

    duration: 1,

    hifi: service(),

    fastForward: 0,

    didInsertElement(){

        let audio = document.getElementById("audio");
        this.set("audioPlayer",audio);

    },

    actions:{
        onChange(selectedTime) {
            let time = moment(selectedTime).format("HH:mm:ss");
            let day = moment();
            let newDate = day.format("YYYY-MM-DD") + " " + time;
            this.set("startTime",newDate);
            this.set("audioDuration", moment.duration(parseInt(this.get("duration")),"minutes").asSeconds())
            let streamUrl = `https://sgrewind.streamguys1.com/rte/radio1/playlist_dvr_range-${ moment(this.get("startTime")).format("X")}-${this.get("audioDuration")}.m3u8`;
            this.set("streamUrl",streamUrl)
            this.set("showPlayer",true)
            this.get('hifi').play(streamUrl);
            console.log
        },
        seekSeconds(direction,seconds){
            let startTime = this.startTime;
            this.set("audioDuration", moment.duration(parseInt(this.get("duration")),"minutes").asSeconds());
            console.log(this.get("duration"))
            let newTime;
            if(direction === "up"){
                newTime = moment(startTime).add(seconds,"seconds");
            }else{
                newTime = moment(startTime).subtract(seconds,"seconds");
            }
            this.set("startTime",newTime);
            let streamUrl = `https://sgrewind.streamguys1.com/rte/radio1/playlist_dvr_range-${ moment(this.get("startTime")).format("X")}-${this.get("audioDuration")}.m3u8`;
            this.set("streamUrl",streamUrl)
            console.log(newTime.format("HH:mm:ss:SSS"));
            this.get("hifi").pause();
            //this.get('hifi').play(this.get("streamUrl"));
        },
        playFromStart(){
            this.set("audioDuration", moment.duration(parseInt(this.get("duration")),"minutes").asSeconds())
            console.log(`set position to ${this.fastForward}`)
            this.get("hifi").set("position",this.fastForward)

            this.get('hifi').play(this.get("streamUrl"));
        },
        pause(){
            this.get("hifi").pause();
        },
        markStart(){
            let seconds = this.get("hifi").position;
            this.set("fastForward",seconds)
            console.log(seconds)
            this.set("audioDuration", moment.duration(parseInt(this.get("duration")),"minutes").asSeconds())
            let newTime = moment(this.get("startTime")).add(seconds,"seconds");
            this.set("startTime",newTime);
            this.set("audioDuration", moment.duration(this.get("duration"),"minutes").asSeconds())
            let streamUrl = `https://sgrewind.streamguys1.com/rte/radio1/playlist_dvr_range-${ moment(newTime).format("X")}-${this.get("audioDuration")}.m3u8`;
            this.set("streamUrl",streamUrl)
            console.log(newTime.format("HH:mm:ss:SSS"));
        }
        
    }
});
