import Component from '@ember/component';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';
//import flowplayer from 'flowplayer';

export default Component.extend({

    ajax: service(),
    //audioUrl: this.get("audioUrl"),
    audioType: "application/x-mpegurl",
    playerReady:false,

    didInsertElement(){
        let fp = document.getElementById("player");
        flowplayer(fp, {
            live: this.get('live'),
            splash: true,
            audioOnly: true,
            autoplay: false,
            clip:{
				splash: true,
				audioOnly: true,
				autoplay: false,
				live: this.get('live'),
				sources : [{
                    type : this.get("audioType"),
                    src: this.get("audioUrl")
				}]
			}       
          });
        /*  
        if(this.get("id") > 100){
            this.set("live",true)
            let url = `https://feeds.rasset.ie/rteavgen/getplaylist/?format=jsonp&id=${this.get("id")}&id=${this.get("id")}&callback=o`;
            this.get("ajax").request(url, {
                dataType: "jsonp"
            }).then(r=>{
                let audioUrl = r.shows[0]["media:group"][0].hls_server + r.shows[0]["media:group"][0].hls_url;
                let duration = r.shows[0]["media:group"][0].duration / 1000;
                this.set("live",false)
                this.set("audioUrl", audioUrl);
                this.set("duration",duration);
                this.set("playerReady",true);
                flowplayer().load({
                    live:false,
                    sources : [{
                        type : this.get("audioType"),
                        src: this.get("audioUrl")
                    }]
                });
            })
        }else{
            this.set("audioUrl","https://cdn.rte.ie/live/ieradio1/playlist.m3u8");
        }
        */

    },

    actions:{
        play(){
            flowplayer().load({
                live:false,
                sources : [{
                    type : this.get("audioType"),
                    src: this.get("audioUrl")
				}]
            });
        },
        toggle(){
            flowplayer().toggle();
        },
        error(error, transition) {
            //transition.send('setFlashMessage', error);
            this.set("error", error);
          }
    }
});
