import Component from '@ember/component';
import moment from 'moment';
import { inject as service } from '@ember/service';
import format from 'format-duration';
import ENV from '../config/environment';

export default Component.extend({

    ajax: service(),

    didInsertElement(){

        //a = `http://localhost/transcoder/builtit.mp3`;
    },

    trimmedAudio: null,
    working: false,
    jobComplete: false,

    checkJobStatus: function(jobId){
        let a = `http://localhost/transcoder/?JobId=${jobId}&callback=doug`;
        let check = setInterval(()=>{
            this.get("ajax").request(a, {
                dataType: "jsonp"
            }).then((r)=>{
                //stopFunction();
                console.log(r); 
                if(r.Status === "Complete"){
                    clearInterval(check);
                    this.set("jobComplete",true);
                }
            }).catch(e=>{
                clearInterval(check);
                console.log(e);
            });
        },1000);

        function stopFunction(){
            clearInterval(check);
        }

    },

    pad: function(num, numZeros) {
        var n = Math.abs(num);
        var zeros = Math.max(0, numZeros - Math.floor(n).toString().length );
        var zeroString = Math.pow(10,zeros).toString().substr(1);
        if( num < 0 ) {
            zeroString = '-' + zeroString;
        }
    
        return zeroString;
    },

    actions:{
        apiRequest(model){
            this.set("working",true)
            let awsUrl = `https://audiofile-trimmed.s3.amazonaws.com/`;
            let start = moment.utc(model.get("starttime")).format("YYYY-MM-DD-HH-mm-ss-SS");
            let end = moment.utc(model.get("endtime")).format("YYYY-MM-DD-HH-mm-ss-SS");
            let service = model.get("service");
            let fileFormat = "mp3";
            let fileName = "temp";
            let audioInTime = model.get("audioInTime"); //sssss.SSS 
            let audioOutTime = model.get("audioOutTime");
            audioOutTime = audioOutTime.toFixed(3);
            audioOutTime = parseFloat(audioOutTime);
            let jsonpCallback = "test";
            let s3UploadUrl = `${ENV.uploaderEndpoint}?start=${start}&end=${end}&service=${service}&format=${fileFormat}&fileName=${fileName}&audioIn=${audioInTime}&audioOut=${audioOutTime}&callback=${jsonpCallback}`;


            this.get("ajax").request(s3UploadUrl, {
                dataType: "jsonp",
                //jsonpCallback: jsonpCallback
            }).then(r=>{
                this.set("working",false);
                let response = r
                console.log(response.body.Job.Output.Key);
                let trimmed = `${awsUrl}${response.body.Job.Output.Key}`;
                this.set("trimmedAudio",trimmed);
                model.set("awsaudio",trimmed);
                model.set("published",true);
                model.save();
                let jobId = response.body.Job.Id;
                this.checkJobStatus(jobId);                
            }).catch(e=>{
                alert("Error!");
            })

        }
    }
});
