import Component from '@ember/component';
import { inject as service } from '@ember/service';
import moment from 'moment';
import { EKMixin, keyDown } from 'ember-keyboard';
import { htmlSafe } from '@ember/string';


export default Component.extend(EKMixin, {
  ajax: service(),

  stations: service(),

  audioDuration: null,

  regionStart: null,

  regionEnd: null,

  isReady: false,

  timeAdded: 0,

  skipLength: 30,

  speed: false,

  showControls: false,

  init() {
    this._super(...arguments);
  },

  didInsertElement() {
    this._super(...arguments);
    this.set("keyboardActivated", true);

    let stationColor = this.get("stations").getStationColor(
      this.get("model").service
    );

    let audiourl = this.get("audiourl");

    const wavesurfer = WaveSurfer.create({
      container: "#waveform",
      waveColor: stationColor,
      progressColor: "#a8b3B3",
      height: "300",
      skipLength: this.get("skipLength"),
      //pixelRatio:1,
      //barWidth: 2,
      //barRadius: 2,
      //normalize:true
    });
    this.set("wavesurfer", wavesurfer);
    wavesurfer.enableDragSelection({
      loop: true,
      color: "rgba(255,74,167,0.3)"
    });

    wavesurfer.load(audiourl);
    wavesurfer.on("error", e => {
      this.set("error", e);
    });
    wavesurfer.on("loading", percents => {
      this.set("percentage", htmlSafe(`width: ${percents}%`));
    });
    wavesurfer.on("ready", () => {
      this.set("isReady", true);
      //set the intial region
      let duration = wavesurfer.getDuration();
      this.set("duration",duration);
      this.on(keyDown("Space"), () => {
        wavesurfer.playPause();
      });

      this.on(keyDown("KeyI"), () => {
        this.send("markStart");
      });

      this.on(keyDown("KeyO"), () => {
        this.send("markEnd");
      });

      this.set("audioDuration", duration);

      document.querySelector("#slider").oninput = function() {
        wavesurfer.zoom(Number(this.value));
      };

      if (!this.model.get("audioInTime")) {
        let regionStart = 1;
        let regionEnd = duration - 1;
        this.set("regionStart", regionStart);
        this.model.set("audioInTime", regionStart.toFixed(3));
        this.model.set("audioOutTime", regionEnd.toFixed(3));
        this.model.save();
      }

      wavesurfer.addRegion({
        start: this.model.get("audioInTime"), // time in seconds
        end: this.model.get("audioOutTime"), // time in seconds
        color: "rgba(255,74,167,0.3)",
        drag: false
      });

      let playButton = document.getElementById("wavesurferPlay");

      playButton.addEventListener("click", function() {
        wavesurfer.play();
      });

      let stopButton = document.getElementById("stop");

      stopButton.addEventListener("click", () => {
        wavesurfer.stop();
      });

      let pauseButton = document.getElementById("pause");
      /*
			pauseButton.addEventListener("click", () => {
				wavesurfer.pause();
			});
			*/

      let playStartButton = document.getElementById("playStart");

      playStartButton.addEventListener("click", e => {
        var percentage =
          this.model.get("audioInTime") / wavesurfer.getDuration();
        wavesurfer.seekTo(percentage);
        wavesurfer.play();
      });

      let playEndButton = document.getElementById("playEnd");

      playEndButton.addEventListener("click", () => {
        let end = this.model.get("audioOutTime");
        var endOffset = this.model.get("audioOutTime") - 2;
        var percentage = endOffset / wavesurfer.getDuration();
        wavesurfer.seekTo(percentage);
        wavesurfer.play(endOffset, end);
      });
    });

    wavesurfer.on("region-created", function(region, e) {
      wavesurfer.clearRegions();
    });

    wavesurfer.on("region-update-end", (region, e) => {
      this.model.set("audioInTime", region.start.toFixed(3));
      this.model.set("audioOutTime", region.end.toFixed(3));

      this.model.save();
    });
    wavesurfer.on("play", () => {
      this.set("playing", true);
    });
    wavesurfer.on("pause", () => {
      this.set("playing", false);
    });
  },
  actions: {
    addTime2(model, where, seconds) {
      this.toggleAction();

      if (where === "starttime") {
        this.model.set(
          where,
          new Date(moment(model.get(where)).subtract(seconds, "seconds"))
        );
      } else {
        this.model.set(
          where,
          new Date(moment(model.get(where)).add(seconds, "seconds"))
        );
      }
      model.save().then(() => {
        this.toggleAction();
      });
    },
    addTime(model, where) {
      this.toggleAction();
      if (where === "starttime") {
        this.model.set(
          where,
          new Date(moment(model.get(where)).subtract(10, "seconds"))
        );
      } else {
        this.model.set(
          where,
          new Date(moment(model.get(where)).add(10, "seconds"))
        );
      }
      this.model.save().then(() => {
        this.toggleAction();
      });
    },
    clearSelection() {
      this.get("wavesurfer").clearRegions();
      this.model.set("audioInTime", 0);
      this.model.set(
        "audioOutTime",
        this.get("wavesurfer")
          .getDuration()
          .toFixed(3)
      );
      this.set("newStart", null);
      this.model.save();
    },
    markStart() {
      this.set("newStart", this.get("wavesurfer").getCurrentTime());
    },
    markEnd() {
      this.set("newEnd", this.get("wavesurfer").getCurrentTime());
      this.get("wavesurfer").addRegion({
        start: this.get("newStart"), // time in seconds
        end: this.get("newEnd"), // time in seconds
        drag: false,
        color: "rgba(255,74,167,0.3)"
      });
      this.model.set("audioInTime", this.get("newStart").toFixed(3));
      this.model.set("audioOutTime", this.get("newEnd").toFixed(3));

      this.model.save();
    },
    resetZoom() {
      this.get("wavesurfer").zoom(20);
      this.get("wavesurfer").scrollParent(false);
    },
    skip(direction) {
      if (direction === "forward") {
        this.get("wavesurfer").skipForward();
      } else {
        this.get("wavesurfer").skipBackward();
      }
    },
    scrub(speed) {
      this.get("wavesurfer").setPlaybackRate(speed);
      this.get("wavesurfer").play();
      this.toggleProperty("speed");
    },
    jumpToEnd() {
      let endOffset = this.get("wavesurfer").getDuration() - 5;
      this.get("wavesurfer").play(endOffset);
    },
    play() {
      this.get("wavesurfer").play();
    },
    togglePlayer() {
      this.get("wavesurfer").playPause();
      //this.get('wavesurfer').play();
    },
    toggleControls() {
      this.toggleProperty("showControls");
    }
  }
});