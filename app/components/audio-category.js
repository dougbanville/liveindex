import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
import { task } from 'ember-concurrency';

export default Component.extend({

    firebaseApp: service(),
    store: service(),

    categories: task(function*() {
        return yield this.get('store').findAll('category',{});
      }).on('init'),

    actions: {
        categorise(c){
            alert("ok" + c.id + " " + this.get("model").title);
            let audioId = this.get("model").id;
            let title = this.get("model").title;
            let awsaudio = this.get("model").awsaudio;
            let awsUploadComplete = this.get("model").awsUploadComplete;
            let published = this.get('model').published;
            let db = this.get('firebaseApp').database();

            let audioObject = this.get("model");

            
            
            db.ref(`audioInCategories/${c.id}/${audioId}`).set({
                title: title,
                awsaudio: awsaudio,
                awsUploadComplete: awsUploadComplete,
                published: published
            })
            
           

        }
    }


});
