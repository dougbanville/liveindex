import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { bind } from '@ember/runloop';

export default Component.extend({

    firebaseApp: service(),
    storageRef: null,
    file: null,

    actions: {
        saveData() {
            const model = this.get("model");
            model.save();
        },
        didSelectImage(files) {
            const model = this.get("model");
            let fileName = model.get("id");
            let folderName =  moment().format("YYYY-MM-DD");


            let reader = new FileReader();
            // Ember.run.bind
            reader.onloadend = bind(this, function () {
                var dataURL = reader.result;
                var output = document.getElementById("output");
                output.src = dataURL;
                this.set("file", files[0]);
                var ext = this.get("file").name.split('.').pop();

                fileName = `/uploads/${folderName}/${fileName}.${ext}`;
                console.log(fileName);

                var storageRef = this.get("firebaseApp").storage().ref();

                var metadata = {
                    contentType: `image/${ext}`
                    };

                var uploadTask = storageRef.child(fileName).put(this.get("file"), metadata);

                uploadTask.on("state_changed", function(snapshot){
                    var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                    console.log(`Upload is ‘ + progress + ‘% done`);
                    console.log(snapshot.state);
                    }, function(error) {
                    }, function() {
                    var downloadURL = uploadTask.snapshot.downloadURL;
                    console.log(downloadURL);
                    model.set("picture",downloadURL);
                    model.save();

                    });


            });
            //debugger;
            reader.readAsDataURL(files[0]);

        },
    }
});
