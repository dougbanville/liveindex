import Component from "@ember/component";

export default Component.extend({
  didRender() {
    let header = document.getElementById("header");
    header.style.display = "none";
  }
});
