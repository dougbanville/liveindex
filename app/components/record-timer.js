import Component from '@ember/component';
import { task, timeout } from 'ember-concurrency';
import moment from 'moment';

export default Component.extend({

    timer: task(function *(){
        let time = moment().startOf("hour").subtract(1,"second");
        //time = time.subtract(1,"second");
       
        while(true){
            this.set("time",time.add(1,"second").format("mm:ss"));
            yield timeout(1000);
        }
    }).on('didInsertElement')

});
