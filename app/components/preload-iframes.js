import Component from "@ember/component";
import $ from "jquery";
import { inject as service } from "@ember/service";

export default Component.extend({
  currentSlide: 0,

  interval: 9000,

  display: service(),

  didInsertElement() {
    this._super(...arguments);
    this.display.toggleShow(true);

    //var mySwiper = new Swiper('.swiper-container', { /* ... */ });
    //model.forEach(r => {});
    let container = $(".if-container");
    this.set("activeFrame", this.model.firstObject.get("id"));

    $(`#slide${this.currentSlide}`).fadeIn("slow");
    setInterval(() => {
      console.clear();
      this.set("activeFrame", "-LZ_mXjElUe4ukFZ6On4");
      $(`#slide${this.currentSlide}`).hide();
      let next = this.currentSlide + 1;
      let len = this.model.length - 1;
      console.log(len);
      if (this.currentSlide >= len) {
        next = 0;
      }

      this.set("currentSlide", next);
      $(`#slide${this.currentSlide}`).show();
    }, this.interval);
  },

  actions: {
    iframeLoaded() {
      console.clear();
      console.log(`first frame has loaded`);
      this.set("loaded", true);
    }
  }
});
