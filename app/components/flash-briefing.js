import Component from '@ember/component';
import moment from 'moment';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';
import ENV from '../config/environment';
import { task, timeout } from 'ember-concurrency';

export default Component.extend({

    store: service(),
    ajax: service(),
    push: service(),
    stations: service(),

    showAudioPlayer:false,

    checkAudioUpToDate: task(function *(){
        //keep checking we are up to date in case they leave browser open
        let notificationSent = true;
        while(true){
            let latest = moment(this.get("model").lastUpdated);
            let gap = yield moment().diff(latest.startOf("Hour"),"minutes");
            if(gap > 65){
                this.set("needsUpdate",true);
                
                if(!notificationSent){

                    let body = `Notification`;
                    let icon = `https://lh5.ggpht.com/ZHrKRvpiLuDpAzK55_VJUXf0g22TM_jHWb5fMG1GUSnt6NtFVoxzlDvqjbi_f7005dU=w300`;
                    let timeout = 5000;       
            
                    this.get('push').create("Hllow", {
                        body, icon, timeout
                      })
                      notificationSent = false;
                    

                }
            }else{
                this.set("needsUpdate",false);
            }

            yield timeout(3000);
        }
        

    }).on('didInsertElement'),

    videoPrefix: `http://radiodj.rte.ie/media/flashbriefing/`,

    latestStart: computed(function(){
        return moment().startOf("hour");
    }),

    latestEnd: computed("latestStart",function(){
        return moment().startOf("hour").add(5,"minutes");
    }),

    unpublishAudio: task(function*(model){
        //Rollback to previous version 
        this.set("showAudioPlayer",false);
        let audioClip = yield this.get("store").findRecord("audioclip",model.get("uid"));

        let category = audioClip.get("category");

        yield audioClip.destroyRecord();

        let nextClip = yield this.get("store").query('audioclip',{
            orderBy: 'category',
            equalTo: category,
            limitToLast: 1
        });
        model.set("streamUrl", nextClip.get("firstObject").get("awsaudio"));
        model.set("uid", nextClip.get("firstObject").get("id"));
        model.set("lastUpdated", nextClip.get("firstObject").get("endtime"));
        model.set("fileSize", nextClip.get("firstObject").get("fileSize"));
        model.set("duration", nextClip.get("firstObject").get("duration"));


        yield model.save();

        this.set("rolling", true)
        //yield console.log(model.get("id"));
        let jsonpCallback = "x";
        let url = `${ENV.transcoderEndpoint}transcoder/updateFlashBriefing.php?category=${model.get("id")}&callback=${jsonpCallback}`;
        this.get("ajax").request(url,{
            dataType: "jsonp",
            jsonpCallback: jsonpCallback
        }).then(r=>{
            this.set("rolling",false);
            this.refreshRoute()
        })
    }),


    didInsertElement(){
        this._super(...arguments);
        this.set("sortOrder",['created:desc']);

    },


    actions:{
        makeFlashAudio(){
            var newAudioClip = this.get("store").createRecord('audioclip', {
				starttime:  new Date(this.get("latestStart")),
                endtime: new Date(this.get("latestEnd")),
                title: this.get("model").titleText,
                category: this.get("model").id,
                service: this.get("stations").get("selectedStation").station_name,
                picture: this.get("model").picture,
                flashBriefing: true
            });

            newAudioClip.save().then(r=>{
                //this.transitionToRoute('audio', r.id)
                this.redirctAction(r.id);
            })
        },
        publishVideo(model,videoSourceFile){
            this.set("savingVideo",true);
            let audioId = model.get("id");            
            let jsonpCallback ="x";
            let s3UploadUrl = `${ENV.transcoderEndpoint}transcoder/saveVideo.php?category=${audioId}&fileName=${videoSourceFile}&callback=${jsonpCallback}`;

            model.set("videoSourceFile",videoSourceFile);

            model.save().then(_=>{
                this.get("ajax").request(s3UploadUrl, {
                    dataType: "jsonp",
                    jsonpCallback: jsonpCallback
                }).then(r=>{
                    this.set("savingVideo",false);
                }).catch(e=>{
                })
           })
           
        }
    }
});
