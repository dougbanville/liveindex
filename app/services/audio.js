import Service from '@ember/service';

export default Service.extend({


    currentTime: 0,

    updateTime(time){
        this.get("audio").setError()
        this.set("currentTime",parseFloat(time));
    },

    setError(e){
        this.set("error",e)
    },

    loadingScreenTakeOver(takeover){
        this.set("takeOver",takeover);
    }
});
