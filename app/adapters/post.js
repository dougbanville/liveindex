import DS from 'ember-data';
import $ from 'jquery';
import { inject as service } from "@ember/service";
import { computed } from "@ember/object";
import ENV from '../config/environment';

export default DS.RESTAdapter.extend({

  defaultSerializer: '-default',

  /*
  init() {
    this._super(...arguments);

    this.set('headers', {
      'API_KEY': 'secret key',
      'ANOTHER_HEADER': 'Some header value'
    });
  },
  */
 host: ENV.transcoderEndpoint,

  headers: computed(function() {
    return {
       "client-security-token": "yyyyyy"
    };
  }),

  pathForType(){
    return  "posts"
  },
});