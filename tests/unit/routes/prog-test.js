import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | prog', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:prog');
    assert.ok(route);
  });
});
