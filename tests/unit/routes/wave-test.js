import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | wave', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:wave');
    assert.ok(route);
  });
});
