let json = require("./response.json");
const admin = require("firebase-admin");
var serviceAccount = require("./radio-a8e0f-firebase-adminsdk-8ykuy-9a9fd0e88d.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://radio-a8e0f.firebaseio.com"
  });

var db = admin.database();


let objectKey = "2018110115002086radio1_p.mp3";

var audioRef = db.ref("audioclips");

/*
audioRef.orderByChild("awsUploadComplete").equalTo(true).limitToFirst(1).on("child_added", (snapshot) => {
  console.log(snapshot.key);

  let id = snapshot.key;


});
*/
/*
audioRef.orderByChild("awsKey").equalTo(objectKey).on("child_added", (snapshot) => {
  console.log(snapshot.key);

  let id = snapshot.key;
  console.log(snapshot.val().published);

  if(snapshot.val().published){
    console.log("good to go")
  }else{
    console.log("not so much")
  }

  let readyOrNot = `true_${snapshot.val().published}`;

  audioRef.child(id).update({
    awsUploadComplete: true,
    ready: readyOrNot
  });

});
*/
/*
audioRef.orderByChild("published").equalTo("it-says-in-the-papers").limitToFirst(1).on("child_added", (snapshot) => {
  console.log(snapshot.key);

  let id = snapshot.key;
  let clip = snapshot.val();

  let object = {
    uid: id,
    updateDate: clip.starttime,
    titleText: clip.title,
    mainText: "",
    streamUrl: clip.awsaudio,
    redirectionUrl: "https://www.rte.ie/"
  }

  console.log(object)


})
*/
let audio = audioRef.orderByChild("published").equalTo("weather").limitToFirst(1).once('value');

audio.then(r => {
  let id = r.key;
  let clip = r.val();
  let keys = Object.keys(clip);
  let first = keys[0];
  console.log(keys[0])
  let object = {
    uid: first,
    updateDate: clip[first].starttime,
    titleText: clip[first].title,
    mainText: "",
    streamUrl: clip[first].awsaudio,
    redirectionUrl: "https://www.rte.ie/"
  }
  console.log(object);
  return
}).catch(e => {
  console.log(e)
})


