const functions = require("firebase-functions");
const moment = require("moment");
const admin = require("firebase-admin");
const https = require("https");
const MessageValidator = require("sns-validator");
var FX = require("impossiblefx");
var SETTINGS = require("./settings");
const cors = require('cors')({origin: true});
const express = require('express');
const app = express();
const mustacheExpress = require('mustache-express');

admin.initializeApp(functions.config().firebase);

exports.awsSns = functions.https.onRequest((request, response) => {
  //admin.initializeApp(functions.config().firebase);
  var db = admin.database();

  var awsRef = db.ref("awsLog");

  let recHeaders = request.headers;

  let log = JSON.stringify(request.body);

  validator = new MessageValidator();

  validator.encoding = "utf8";

  validator.validate(request.body, (err, m) => {
    if (err) {
      console.error(err);
      return;
    }

    if (m["Type"] === "Notification") {
      // Do whatever you want with the message body and data.
      //console.log(m['MessageId'] + ': ' + m['Message']);
      var id = moment().format("YYYYMMDDhisS");
      let messageJson = m["Message"];
      messageJson = JSON.parse(messageJson);
      console.log(messageJson.Records[0].s3.object.key);
      let objectKey = messageJson.Records[0].s3.object.key;

      awsRef.set({
        log: {
          body: messageJson.Records[0].s3.object.key
        }
      });

      awsRef.once("value", snapshot => {
        console.log(snapshot.val());
      });

      var audioRef = db.ref("audioclips");

      audioRef
        .orderByChild("awsKey")
        .equalTo(objectKey)
        .on("child_added", snapshot => {
          console.log(snapshot.key);

          let id = snapshot.key;
          let category = snapshot.val().category;

          audioRef.child(id).update({
            awsUploadComplete: true,
            published: category
          });
        });
    }
  });

  console.log(recHeaders["x-amz-sns-message-type"]);
  response.send("<b>Hello from Firebase!</b>");
});

exports.deleteAudioFiles = functions.database
  .ref("audioclips/{id}")
  .onDelete((snapshot, context) => {
    const original = snapshot.val();
    console.log(`Deleted Audioclip`);
  
});

FX.config.apikey = SETTINGS.impossible.apikey;
FX.config.apisecret = SETTINGS.impossible.apisecret;
FX.config.region = SETTINGS.impossible.region;
FX.config.apiVersion = SETTINGS.impossible.apiVersion;

exports.makeVideo = functions.https.onRequest((request, response) => {

  cors(request, response, () => {

    const audio = decodeURI(request.query.audio);
    const picture = decodeURI(request.query.picture);
    const message = decodeURI(request.query.message);
  
    const projectName = "doug";
    const projectId = "170b120b-a8bc-497b-9a21-7b7697954a18";
  
    const sdl = new FX.SDL();
  
    var prj = new FX.Project({
      params: {
        ProjectId: projectId
      }
    });
  
    var backgroundVideo = new sdl.VisualTrack({
      content: {
        type: "video",
        source: {
          path: "userdata/706896863.mp4"
        }
      }
    });

    var Sting = new sdl.VisualTrack({
      numframes: 35,
      content: {
        type: "video",
        source: {
          path: "userdata/radio1-sting.m4v"
        }
      }
    });
  
    var intro = new sdl.VisualTrack({
      numframes: 500,
      content: {
        type: "http",
        source: {
          type: "var",
          variable: {
            type: "map",
            key: "pic",
            defaultvalue: picture
          },
          path: "http://$variable"
        }
      }
    });
  
    var foregroundTrack = new sdl.VisualTrack({
      numframes: 500,
      content: {
        type: "textsimple",
        fontsize: 40,
        color: {
          red: 255,
          green: 255,
          blue: 255,
          alpha: 255
        },
        xalignment: "centered",
        yalignment: "bottom",
        text: {
          type: "map",
          key: "message",
          defaultvalue: message
        }
      }
    });
  
    var introAudio = new sdl.AudioTrack({
      type: "video_mix",
      usecachedir: false,
      source: {
        path:
          audio
      }
    });
  
    var introScene = new sdl.Scene({
      tracks: [backgroundVideo,  intro, Sting, foregroundTrack],
      audio: {
        audiotracks: [introAudio]
      },
      useaudioforlength: true
    });
  
    var movie = new sdl.Movie({
      params: {
        vparams: {
          videocodec: "VIDEO_X264",
          width: 640,
          height: 360,
          videoframerate: {
            num: 25,
            den: 1
          },
          videobitrate: 1000,
          videoqp: 22,
          videorc: "VRC_RATEFACTOR",
          videocpueffort: 20
        },
        aparams: {
          audiosamplerate: 44100,
          audiocodec: "AUDIO_AAC",
          audioframe_size: 1152
        }
      },
      scenes: [introScene]
    });
    //console.log(movie);
    makeMovie = prj.createMovie(
      {
        Name: projectName,
        Movie: movie
      },
      (err, data) => {
        let object = {};
        if (err) {
          console.log(err.stack);
          object.error = err.stack
        }
        object.success = "Movie created:" + data.Message + prj;
        //console.log(prj)
        //return response.json(object);
      }
    )
  
    var parameters = {
      ProjectId: projectId, // required
      Movie: projectName, // required
      Params: {pic: picture, message: message}, // required
      Endpoint: `https://render-eu-west-1.impossible.io/v2/render/170b120b-a8bc-497b-9a21-7b7697954a18`
    }
  
    let promise = makeMovie.promise();
  
    return promise.then(r=>{
      console.log(r);
      //console.log(parameters)
      videoUrl =  new FX.Render().getRenderURL(parameters);
      return videoUrl.promise()
    }).then(u=>{
      console.log(u.Token);
      u.videoUrl = `https://render-eu-west-1.impossible.io/v2/render/${u.Token}.mp4`;
      return response.json(u)
    }).catch(err=>{
      console.log(`ERROR ${err}`);
    })


  });

});


app.engine('html', mustacheExpress());
app.use(cors);
app.set('view engine', 'mustache');
app.set('views', __dirname + '/views');
app.get('/:id', (req, res) => {
  let db = admin.database();
  let audio = db.ref(`audioclips/${req.params.id}`);
  
  audio.once("value", snapshot => {
    console.log(snapshot.val());
    let detail = snapshot.val();
    if(detail.videoUrl){
      video = true;
    }else{
      video = false;
    }
    //res.send(`Hello World! ${detail.title}`);
    res.render('socialTags.html', {id: snapshot.key, title: detail.title, picture: detail.picture, caption: detail.caption, category: detail.category, audio: detail.awsaudio, video: detail.generatedVideo      , hasVideo: video
    })
  });


});

exports.socialTags = functions.https.onRequest(app);

app.engine('html', mustacheExpress());
app.use(cors);
app.set('view engine', 'mustache');
app.set('views', __dirname + '/views');
app.get('/:id', (req, res) => {
  let db = admin.database();
  let audio = db.ref(`audioclips/${req.params.id}`);
  
  audio.once("value", snapshot => {
    console.log(snapshot.val());
    let detail = snapshot.val();
    if(detail.videoUrl){
      video = true;
    }else{
      video = false;
    }
    //res.send(`Hello World! ${detail.title}`);
    res.render('twitterPlayer.html', {id: snapshot.key, title: detail.title, picture: detail.picture, caption: detail.caption, category: detail.category, audio: detail.awsaudio, video: detail.generatedVideo
    })
  });
});
exports.twitterPlayer = functions.https.onRequest(app);


