"use strict";

module.exports = function(environment) {
  let ENV = {
    modulePrefix: "live-index",
    environment,
    rootURL: "/",
    locationType: "auto",
    //uploaderEndpoint:"http://localhost/transcoder",
    uploaderEndpoint: "https://radioapps.rte.ie/banvilled/audiofile-ui/",
    audioFileEnpoint: "http://audiofile.rte.ie",
    apiGatewayKey: process.env.apiGateWayKey,
    twKey: process.env.twKey,
    transcoderEndpoint: `http://localhost/`,
    torii: {
      sessionServiceName: "session"
    },
    firebase: {
      apiKey: process.env.apiKey,
      authDomain: process.env.authDomain,
      databaseURL: process.env.databaseURL,
      storageBucket: process.env.storageBucket,
      projectId: process.env.projectId
    },
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      },
      EXTEND_PROTOTYPES: {
        // Prevent Ember Data from overriding Date.parse.
        Date: false
      }
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    }
  };

  if (environment === "development") {
    ENV.uploaderEndpoint = "http://localhost/transcoder";
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
  }

  if (environment === "test") {
    // Testem prefers this...
    ENV.locationType = "none";

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = "#ember-testing";
    ENV.APP.autoboot = false;
  }

  if (environment === "production") {
    // here you can enable a production-specific feature
    //ENV.rootURL = "/timewave/";
    ENV.transcoderEndpoint = "http://dub-lx-107.rtegroup.ie/";
    //ENV.audioFileEnpoint = "";

    // here you can enable a production-specific feature
  }

  return ENV;
};
